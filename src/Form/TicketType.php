<?php

namespace App\Form;

use App\Entity\CustomerProduct;
use App\Entity\Ticket;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Validator\Constraints\File;

class TicketType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $user = $options['user'];
        $builder
            ->add('name', TextType::class, [
                'required' => true,
                'attr' => ['placeholder' => '...', 'maxlength' => '255']
            ])
            ->add('description',TextareaType::class, [
                'required' => true,
                'attr' => ['placeholder' => '...',]
            ])
            ->add('picture', FileType::class, [
                'mapped' => false,
                'constraints' => [
                    new File([
                        'maxSize' => '5M',
                        'mimeTypes' => [
                            'image/*',
                        ],
                        'mimeTypesMessage' => 'Le fichier n\'est pas valide, assurez vous d\'avoir un fichier au format PNG, JPG, JPEG)',
                    ]),
                ]
            ])
            ->add('customerProduct', EntityType::class, [
                'class' => CustomerProduct::class,
                'choice_label' => 'nr_serie',
                'required' => true,
                'placeholder' => 'Sélectionner un produit',
                'attr' => ['class' => 'form-control'],
                'query_builder' => function (EntityRepository $er) use ($user) {
                    return $er->createQueryBuilder('cp')
                        ->andWhere('cp.user = :user')
                        ->setParameter('user', $user);
                }
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Ticket::class,
        ]);
        $resolver->setRequired('user'); // specify that the "user" option is required
        $resolver->setAllowedTypes('user', 'App\Entity\User'); // specify the type of the "user" option
    }
}
