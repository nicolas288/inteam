<?php

namespace App\Form;

use App\Entity\Product;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Validator\Constraints\File;

class ProductType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', TextType::class, [
                'constraints' => [
                    new Length([
                        'min' => 4,
                        'minMessage' => 'Veuillez remplir un nom d\'au moins {{ limit }} caractères',
                        'max' => 255,
                        'maxMessage' => 'Veuillez remplir un nom de moins de {{ limit }} caractères',
                    ]),
                ]
            ])
            ->add('picture', FileType::class, [
                'mapped' => false,
                'constraints' => [
                    new File([
                        'maxSize' => '5M',
                        'mimeTypes' => [
                            'image/*',
                        ],
                        'mimeTypesMessage' => 'Le fichier n\'est pas valide, assurez vous d\'avoir un fichier au format PNG, JPG, JPEG)',
                    ]),
                ]
            ])
            ->add('marque', ChoiceType::class, [
                'choices'  => [
                    'Marque 1' => 'Marque 1',
                    'Marque 2' => 'Marque 2',
                    'Marque 3' => 'Marque 3'
                ],
            ])
            ->add('model', TextType::class, [
                'constraints' => [
                    new Length([
                        'min' => 2,
                        'minMessage' => 'Veuillez remplir un model d\'au moins {{ limit }} caractères',
                        'max' => 255,
                        'maxMessage' => 'Veuillez remplir un model de moins de {{ limit }} caractères',
                    ]),
                ]
            ])
            ->add('gamme', TextType::class, [
                'constraints' => [
                    new Length([
                        'min' => 2,
                        'minMessage' => 'Veuillez remplir une gamme d\'au moins {{ limit }} caractères',
                        'max' => 255,
                        'maxMessage' => 'Veuillez remplir une gamme de moins de {{ limit }} caractères',
                    ]),
                ]
            ])
            ->add('type', ChoiceType::class, [
                'choices'  => [
                    'Tapis de courses' => 'Tapis de courses',
                    'Vélo' => 'Vélo',
                    'Rameur' => 'Rameur'
                ],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Product::class,
        ]);
    }
}
