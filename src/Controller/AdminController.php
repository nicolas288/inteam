<?php

namespace App\Controller;

use App\Entity\Product;
use App\Entity\Task;
use App\Entity\Ticket;
use App\Form\ProductType;
use App\Form\TaskType;
use App\Form\TicketAdminType;
use App\Repository\TicketRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[IsGranted("ROLE_USER")]
class AdminController extends AbstractController {

    private $em;

    /**
     * @param EntityManagerInterface $em
     */
    public function __construct(
        EntityManagerInterface $em
    )
    {
        $this->em = $em;
    }

    #[Route('/', name:'admin_index')]
    public function index(TicketRepository $ticketRepository, UserRepository $userRepository) {
        $user = $this->getUser();
        if (in_array('ROLE_CUSTOMER', $user->getRoles())) {
            return $this->redirectToRoute('customer_index');
        } else if (in_array('ROLE_MANTAINER', $user->getRoles())) {
            return $this->redirectToRoute('tech_index');
        }
        $tickets = $ticketRepository->findAll();
        $customers = $userRepository->getCustomers();
        $mantainers = $userRepository->getMantainers();
        return $this->render('admin/index.html.twig',
        [
            'tickets' => $tickets,
            'customers' => $customers,
            'mantainers' => $mantainers,
            'userName' => $user->getName()
        ]);
    }

    #[IsGranted('ROLE_ADMIN')]
    #[Route('/product/new', name:'admin_new_product')]
    public function newProduct(Request $request) {
        $product = new Product();
        $form = $this->createForm(ProductType::class, $product);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $product->setPicture("en attente");

            $this->em->persist($product);
            $this->em->flush();

            $uploadedFile = $form['picture']->getData();
            $destination = $this->getParameter('kernel.project_dir').'/public/uploads';
            $newFilename = 'product' . $product->getId() . "_picture.".$uploadedFile->guessExtension();

            $uploadedFile->move(
                $destination,
                $newFilename
            );
            $product->setPicture($newFilename);
            $this->em->flush();

            return $this->redirectToRoute('admin_product_task', ['id' => $product->getId()]);
        }

        return $this->render('admin/newProduct.html.twig', [
            'form' => $form->createView(),
            'userName' => $this->getUser()->getName()
        ]);
    }

    #[IsGranted('ROLE_ADMIN')]
    #[Route('/product/{id}/task', name:'admin_product_task')]
    public function productTask(Product $product, Request $request) {
        $task = new Task();
        $form = $this->createForm(TaskType::class, $task);

        $taskRepository = $this->em->getRepository(Task::class);
        $tasks = $taskRepository->findBy(['product' => $product]);
        $lastTask = end($tasks);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $task->setProduct($product);
            if($lastTask){
                $task->setHierarchy($lastTask->getHierarchy() + 1);
            }else{
                $task->setHierarchy(1);
            }

            $this->em->persist($task);
            $this->em->flush();

            return $this->redirectToRoute('admin_product_task', ['id' => $product->getId()]);
        }

        return $this->render('admin/productTask.html.twig', [
            'form' => $form->createView(),
            'userName' => $this->getUser()->getName(),
            'tasks' => $tasks,
            'taskCount' => count($tasks)+1
        ]);
    }

    #[IsGranted('ROLE_ADMIN')]
    #[Route('/task/{id}/delete', name:'admin_product_task_delete')]
    public function productTaskDelete(Task $task, Request $request) {
        $submittedToken = $request->request->get('token');
        $product = $task->getProduct();

        if ($this->isCsrfTokenValid('delete-task'.$task->getId(), $submittedToken)) {
            $this->em->remove($task);
            $this->em->flush();
        }

        return $this->redirectToRoute('admin_product_task', ['id' => $product->getId()]);
    }

    #[IsGranted('ROLE_ADMIN')]
    #[Route('/ticket/{id}/update', name:'admin_ticket_update')]
    public function ticketUpdate(Ticket $ticket, Request $request) {
        $form = $this->createForm(TicketAdminType::class, $ticket);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()){
            $this->em->flush();
            return $this->redirectToRoute('admin_ticket_update', ['id' => $ticket->getId()]);
        }

        return $this->render('admin/updateTicket.html.twig', [
            'form' => $form->createView(),
            'ticket' => $ticket,
            'userName' => $this->getUser()->getName()
        ]);
    }
}
