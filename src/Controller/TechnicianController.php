<?php


namespace App\Controller;

use App\Entity\Ticket;
use App\Repository\TicketRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

class TechnicianController extends AbstractController
{
    #[IsGranted('ROLE_MANTAINER')]
    #[Route('/mantainer', name:'tech_index')]
    public function index(TicketRepository $ticketRepository) {
        $user = $this->getUser();
        $tickets = $ticketRepository->findBy(['mantainer' => $user]);
        return $this->render('technician/index.html.twig',[
            'tech' => $user,
            'tickets' => $tickets,
            'userName' => $this->getUser()->getName()
        ]);
    }
}
