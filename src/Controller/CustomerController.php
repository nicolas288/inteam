<?php

namespace App\Controller;

use App\Entity\Ticket;
use App\Form\TicketType;
use App\Repository\TicketRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

class CustomerController extends AbstractController {

    #[IsGranted('ROLE_CUSTOMER')]
    #[Route('/customer', name:'customer_index')]
    public function index(TicketRepository $ticketRepository): Response
    {
        $user = $this->getUser();
        $tickets = $ticketRepository->getTicketsByCustomer($user);
        return $this->render('customer/index.html.twig',[
            'userSiren' => $user,
            'tickets' => $tickets,
            'userName' => $this->getUser()->getName()
        ]);
    }

    #[IsGranted('ROLE_CUSTOMER')]
    #[Route('/ticket/add', name:'add_ticket')]
    public function addTicket(Request $request, EntityManagerInterface $entityManager) : Response
    {
        $user = $this->getUser();
        $ticket = new Ticket;
        $form = $this->createForm(TicketType::class, $ticket, ['user' => $user]);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()){
            $ticket->setPicture("en attente");
            $ticket->setStatus("0");
            $entityManager->persist($ticket);
            $entityManager->flush();

            $uploadedFile = $form['picture']->getData();
            $destination = $this->getParameter('kernel.project_dir').'/public/uploads';
            $newFilename = 'ticket' . $ticket->getId() . "_picture.".$uploadedFile->guessExtension();
            $uploadedFile->move(
                $destination,
                $newFilename
            );
            $ticket->setPicture($newFilename);
            $entityManager->flush();

            return $this->redirectToRoute('customer_index');
        }

        return $this->render('customer/ticketForm.html.twig', [
            'form' => $form->createView(),
            'userName' => $this->getUser()->getName()
        ]);
    }

    #[IsGranted('ROLE_USER')]
    #[Route('/ticket/{id}', name:'technician_ticket')]
    public function ticketView(Ticket $ticket) {
        return $this->render('technician/ticket.html.twig', [
            'ticket' => $ticket,
            'userName' => $this->getUser()->getName()
        ]);
    }
}
