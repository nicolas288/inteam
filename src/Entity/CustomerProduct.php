<?php

namespace App\Entity;

use App\Repository\CustomerProductRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: CustomerProductRepository::class)]
class CustomerProduct
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $nr_serie = null;

    #[ORM\ManyToOne(inversedBy: 'customerProducts')]
    private ?User $user = null;

    #[ORM\ManyToOne(inversedBy: 'customerProduct')]
    private ?Product $product = null;

    #[ORM\OneToMany(mappedBy: 'customerProduct', targetEntity: Ticket::class)]
    private Collection $tickets;

    #[ORM\OneToMany(mappedBy: 'customerProduct', targetEntity: TaskProductUser::class)]
    private Collection $taskProductUsers;

    public function __construct()
    {
        $this->tickets = new ArrayCollection();
        $this->taskProductUsers = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNrSerie(): ?string
    {
        return $this->nr_serie;
    }

    public function setNrSerie(string $nr_serie): self
    {
        $this->nr_serie = $nr_serie;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getProduct(): ?Product
    {
        return $this->product;
    }

    public function setProduct(?Product $product): self
    {
        $this->product = $product;

        return $this;
    }

    /**
     * @return Collection<int, Ticket>
     */
    public function getTickets(): Collection
    {
        return $this->tickets;
    }

    public function addTicket(Ticket $ticket): self
    {
        if (!$this->tickets->contains($ticket)) {
            $this->tickets->add($ticket);
            $ticket->setCustomerProduct($this);
        }

        return $this;
    }

    public function removeTicket(Ticket $ticket): self
    {
        if ($this->tickets->removeElement($ticket)) {
            // set the owning side to null (unless already changed)
            if ($ticket->getCustomerProduct() === $this) {
                $ticket->setCustomerProduct(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, TaskProductUser>
     */
    public function getTaskProductUsers(): Collection
    {
        return $this->taskProductUsers;
    }

    public function addTaskProductUser(TaskProductUser $taskProductUser): self
    {
        if (!$this->taskProductUsers->contains($taskProductUser)) {
            $this->taskProductUsers->add($taskProductUser);
            $taskProductUser->setCustomerProduct($this);
        }

        return $this;
    }

    public function removeTaskProductUser(TaskProductUser $taskProductUser): self
    {
        if ($this->taskProductUsers->removeElement($taskProductUser)) {
            // set the owning side to null (unless already changed)
            if ($taskProductUser->getCustomerProduct() === $this) {
                $taskProductUser->setCustomerProduct(null);
            }
        }

        return $this;
    }
}
