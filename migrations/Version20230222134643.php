<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230222134643 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE customer_product (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, product_id INT DEFAULT NULL, nr_serie VARCHAR(255) NOT NULL, INDEX IDX_CF97A013A76ED395 (user_id), UNIQUE INDEX UNIQ_CF97A0134584665A (product_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE product (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, picture VARCHAR(255) DEFAULT NULL, marque VARCHAR(255) NOT NULL, model VARCHAR(255) NOT NULL, gamme VARCHAR(255) NOT NULL, type VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE task (id INT AUTO_INCREMENT NOT NULL, task_product_user_id INT DEFAULT NULL, hierarchy INT NOT NULL, name VARCHAR(255) NOT NULL, INDEX IDX_527EDB2530C25F2A (task_product_user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE task_product_user (id INT AUTO_INCREMENT NOT NULL, customer_product_id INT DEFAULT NULL, status TINYINT(1) NOT NULL, comment LONGTEXT NOT NULL, date DATE NOT NULL, INDEX IDX_C8EAD0C5B84D62D8 (customer_product_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ticket (id INT AUTO_INCREMENT NOT NULL, customer_product_id INT DEFAULT NULL, mantainer_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, description LONGTEXT NOT NULL, status INT NOT NULL, final_date DATE NOT NULL, action_date DATE NOT NULL, INDEX IDX_97A0ADA3B84D62D8 (customer_product_id), INDEX IDX_97A0ADA37660F5A (mantainer_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, email VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, adress VARCHAR(255) DEFAULT NULL, commune VARCHAR(255) DEFAULT NULL, department VARCHAR(255) DEFAULT NULL, region VARCHAR(255) DEFAULT NULL, country VARCHAR(255) DEFAULT NULL, phone VARCHAR(255) DEFAULT NULL, siren LONGTEXT DEFAULT NULL, UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE messenger_messages (id BIGINT AUTO_INCREMENT NOT NULL, body LONGTEXT NOT NULL, headers LONGTEXT NOT NULL, queue_name VARCHAR(190) NOT NULL, created_at DATETIME NOT NULL, available_at DATETIME NOT NULL, delivered_at DATETIME DEFAULT NULL, INDEX IDX_75EA56E0FB7336F0 (queue_name), INDEX IDX_75EA56E0E3BD61CE (available_at), INDEX IDX_75EA56E016BA31DB (delivered_at), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE customer_product ADD CONSTRAINT FK_CF97A013A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE customer_product ADD CONSTRAINT FK_CF97A0134584665A FOREIGN KEY (product_id) REFERENCES product (id)');
        $this->addSql('ALTER TABLE task ADD CONSTRAINT FK_527EDB2530C25F2A FOREIGN KEY (task_product_user_id) REFERENCES task_product_user (id)');
        $this->addSql('ALTER TABLE task_product_user ADD CONSTRAINT FK_C8EAD0C5B84D62D8 FOREIGN KEY (customer_product_id) REFERENCES customer_product (id)');
        $this->addSql('ALTER TABLE ticket ADD CONSTRAINT FK_97A0ADA3B84D62D8 FOREIGN KEY (customer_product_id) REFERENCES customer_product (id)');
        $this->addSql('ALTER TABLE ticket ADD CONSTRAINT FK_97A0ADA37660F5A FOREIGN KEY (mantainer_id) REFERENCES user (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE customer_product DROP FOREIGN KEY FK_CF97A013A76ED395');
        $this->addSql('ALTER TABLE customer_product DROP FOREIGN KEY FK_CF97A0134584665A');
        $this->addSql('ALTER TABLE task DROP FOREIGN KEY FK_527EDB2530C25F2A');
        $this->addSql('ALTER TABLE task_product_user DROP FOREIGN KEY FK_C8EAD0C5B84D62D8');
        $this->addSql('ALTER TABLE ticket DROP FOREIGN KEY FK_97A0ADA3B84D62D8');
        $this->addSql('ALTER TABLE ticket DROP FOREIGN KEY FK_97A0ADA37660F5A');
        $this->addSql('DROP TABLE customer_product');
        $this->addSql('DROP TABLE product');
        $this->addSql('DROP TABLE task');
        $this->addSql('DROP TABLE task_product_user');
        $this->addSql('DROP TABLE ticket');
        $this->addSql('DROP TABLE user');
        $this->addSql('DROP TABLE messenger_messages');
    }
}
